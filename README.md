# **Self-organized Text Detection with Minimal Post-processing via Border Learning**
This repo contains two Fully Convolutional Network (FCN) based text detectors with border supports. Provided models are trained with the [**ISI-PPT-Dataset**  git@gitlab.com:rex-yue-wu/ISI-PPT-Dataset.git](https://gitlab.com/rex-yue-wu//ISI-PPT-Dataset), by using the [**Keras**](https://keras.io/), python deep learning library, with the **Theano** backend. Detailed training settings and descriptions can be found in our ICCV17 paper  (see the provided citation at the bottom). 

![sample text detection results.](https://gitlab.com/rex-yue-wu/ISI-PPT-Text-Detector/raw/master/sample_data/sample_dec.png)
# **Installation**
#### Python2
1. Clone this repo to local, namely
    ```
    git clone git@gitlab.com:rex-yue-wu/ISI-PPT-Text-Detector.git
    cd ISI-PPT-Text-Detector
    ```
2. Set up `python2` environment properly, if you have sudo,
    ```
    pip install -r requirements.txt
    ```
    otherwise
    ```
    pip install --user -r requirements.txt
    ```
    both of which will automatically set your enviroment to those specified in the `requirements.txt` file. 
3. Modify your system's `keras.json` file (usually located at `~/.keras/keras.json`) to use the *Theano* backend. A simple solution is to overwrite this file with repo's `keras.json`, namely,
    ```
    mv ~/.keras/keras.json ~/.keras/keras.json.bk
    cp keras.json ~/.keras/keras.json
    ```
4. Specify `THEANO_FLAGS` and run. It is suggested that to use the `CPU` mode for decoding, to avoid the **out-of-memory error**, namely,
    ```
    THEANO_FLAGS=mode=FAST_RUN,device=cpu,floatX=float32 python src/ppt_text_detector.py -h
    ```
    
#### Linux Binary
For non-python users, or users who only want to use the provided text detectors as black-boxes, you may use our pre-compiled linux standalone instead. 
1. Clone this repo to local, namely
    ```
    git clone https://github.com/ISICV/PPT_text_detector_keras.git
    cd PPT_text_detector_keras/bin
    ```
2. Decompress the binary folder, namely 
    ```
    tar -zxvf ppt_text_detector.tar.gz
    ```
3. Call the pre-compiled linux binary in `bin/ppt_text_detector/ppt_text_detector` directly.
4. If you encounter errors when calling this binary, please refer to [`bin/README.md`](https://gitlab.com/rex-yue-wu/ISI-PPT-Text-Detector/blob/master/bin/README.md) for solutions.

# **Usage**
#### Python, ipynb, command-line tool
After installation, you may 
1. call/import internal python functions in `src/{models,utils}.py`.
2. run demo IPython Notebook code in [`src/demo.ipynb`](https://gitlab.com/rex-yue-wu/ISI-PPT-Text-Detector/blob/master/src/demo.ipynb)
3. directly use the provided command-line tool `src/ppt_text_detector.py` or the equivalent standalone `bin/ppt_text_detector/ppt_text_detector`.
   
#### Text Detector API
The detailed command-line **ppt_text_detector** API is specified below
 ```shell
usage: ppt_text_detector.py [-h] [-i INPUT_FILES] [-o OUTPUT_DIR] [-w WEIGHT_FILE] [-v VERBOSE] [-th_size TH_SIZE] [-th_prob TH_PROB] [--no_map] [--no_box] [--version]

    PPT Text Detection with Border Support
    
    Arguments:
      -h, --help            show this help message and exit
      -i INPUT_FILES        input test image files [Mandatory]
      -o OUTPUT_DIR         output detection dir (./)
      -w WEIGHT_FILE        keras model weight file (models/multi_res.h5)
      -v VERBOSE            verbosity level (0), higher means more print outs
      -th_size TH_SIZE      minimal text line dimension (8) in output
      -th_prob TH_PROB      minimal text line probability (.25) in output
      --no_map              not produce text detection response maps
      --no_box              not save individual text bounding boxes to json per image
      --version             show program's version number and exit
```
In short, **ppt_text_detector** takes one/multiple input image files and outputs corresponding text-response maps (unless `no_map` is set) and detected text bounding boxes (unless `no_box` is set).

For each input image `image_file = '/my_full_path_dir/image_prefix.ext'`, **ppt_text_detector** could generate two files under `OUTPUT_DIR`, namely
- `image_prefix.png`: 
    - a decoded probability map, which has the same size as the input image
    - RGB color intensities of each pixel represent the probability of each text classes, namely,
        - ![#ff0000](https://placehold.it/15/ff0000/000000?text=+) `non-text`
        - ![#00ff00](https://placehold.it/15/00ff00/000000?text=+) `border`
        - ![#0000ff](https://placehold.it/15/0000ff/000000?text=+) `text`
        - `pixel_intensity` = `uint8( proba * 255 )`
- `image_prefix.json` : 
    -  a json encoded bounding-box file
    -  it has the following structure:
        - `bounding_box` : 
            a list of detected boxes, [ `text_box_1`, `text_box_2`, ..., `text_box_K` ], where each detected box is represented by four integers, namely, [`box_top`, `box_bot`, `box_left`, `box_right`]. 
        - `proba` : 
            a list of estimated text region probabilities, [`text_proba_1`, `text_proba_2`, ..., `text_proba_K` ], where probability is in the range `(0, 1)` and represented by 4 significant figures.
        - `input_file`:
            the source image file input to the text detector.
        - `response_map` :
            the output probability response map, unless `no_map` is set.

# **Test**
Simply run the snippet below. The newly generated output json file is expected to be identical to the cached one.
```shell
python src/ppt_text_detector.py -i sample_data/fa80cae4ab856cc4a8fd50c7bb1d1b62.jpg
diff fa80cae4ab856cc4a8fd50c7bb1d1b62.json test/fa80cae4ab856cc4a8fd50c7bb1d1b62.json
```

Alternatively, you may run our provided IPython Notebook `src/demo.ipynb`.

# **Tips**
Below are some tips for using the provided  **ppt_text_detector**
1. **ppt_text_detector** may decode multiple input files at once, simply by invoking `-i` multiple times, for example,
    ```shell
    ppt_text_detector -i input_image_file_1.jpg -i input_image_file_2.png -i input_image_file_3.bmp
    ```
2. If you do not want many output files, you can sepecify `--no_box` in **ppt_text_detector**, and it will only a `detected_bbox.json` containing all results for all input images.

3. To achieve better detection performance, you may resize a testing image to match the text font size of training data, namely,
    1. If an input file is **PowerPoint**, **PDF** or **WORD** converted image, aspect-ratio perserving based resize
        - image height to `540` pixels, when orientation is **landscape**
        - image width to `720` pixels, when orientation is **portait**.
    2. The known best fontsize for decoding are:
        - range `(12, 48)` for the pretrained multi-resolution model under 72 dpi
        - see sample images of different fontsizes under `sample_data/synthesized*.jpg`
4. Though provided model also works for rotated text region, you may achieve better performance if you detect text in two-pass, namely 
        1) detect text in the original image,
        2) estimate rotation from the obtained response map and correct rotation
        3) detect text in the rotation-corrected image. 
5. You may start from the generated text probability response map from our text detector, and then write your own response map to text region decoder, because the provided one simply parse horizontal text bounding boxes for each text connected component in the response map, and it is known insufficient in many cases, e.g. rotated text regions (see the last cell of `src/demo.ipynb`). 
6. If you use this text detector for scene-text detection, please expect performance degradation, because no scene-text training data is used for training the pretrained model. Instead, you may finetune the pretrained model on a scene-text dataset. 

# **Citation**
If you use this repo in any publication, please kindly cite our paper below.

    @inproceedings{wu2017iccv,
        author = {Yue Wu and Prem Natarajan},
        booktitle = {International Conference on Computer Vision},
        title = {Self-organized Text Detection with Minimal Post-processing via Border Learning},
        year = {2017}
    }
    
# Contact
- Author: Yue Wu
- Email: yue_wu@isi.edu
- Affiliation: USC Information Sciences Institute, USA



# License
The Software is made available for academic or non-commercial purposes only. The license is for a copy of the program for an unlimited term. Individuals requesting a license for commercial use must pay for a commercial license. 

      USC Stevens Institute for Innovation 
      University of Southern California 
      1150 S. Olive Street, Suite 2300 
      Los Angeles, CA 90115, USA 
      ATTN: Accounting 

DISCLAIMER. USC MAKES NO EXPRESS OR IMPLIED WARRANTIES, EITHER IN FACT OR BY OPERATION OF LAW, BY STATUTE OR OTHERWISE, AND USC SPECIFICALLY AND EXPRESSLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, VALIDITY OF THE SOFTWARE OR ANY OTHER INTELLECTUAL PROPERTY RIGHTS OR NON-INFRINGEMENT OF THE INTELLECTUAL PROPERTY OR OTHER RIGHTS OF ANY THIRD PARTY. SOFTWARE IS MADE AVAILABLE AS-IS. LIMITATION OF LIABILITY. TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT WILL USC BE LIABLE TO ANY USER OF THIS CODE FOR ANY INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR PUNITIVE DAMAGES OF ANY KIND, LOST GOODWILL, LOST PROFITS, LOST BUSINESS AND/OR ANY INDIRECT ECONOMIC DAMAGES WHATSOEVER, REGARDLESS OF WHETHER SUCH DAMAGES ARISE FROM CLAIMS BASED UPON CONTRACT, NEGLIGENCE, TORT (INCLUDING STRICT LIABILITY OR OTHER LEGAL THEORY), A BREACH OF ANY WARRANTY OR TERM OF THIS AGREEMENT, AND REGARDLESS OF WHETHER USC WAS ADVISED OR HAD REASON TO KNOW OF THE POSSIBILITY OF INCURRING SUCH DAMAGES IN ADVANCE. 

For commercial license pricing and annual commercial update and support pricing, please contact: 

      Rakesh Pandit USC Stevens Institute for Innovation 
      University of Southern California 
      1150 S. Olive Street, Suite 2300
      Los Angeles, CA 90115, USA 

      Tel: +1 213-821-3552
      Fax: +1 213-821-5001 
      Email: rakeshvp@usc.edu and ccto: accounting@stevens.usc.edu

